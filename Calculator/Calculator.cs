﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myTests
{
    public class Calculator
    {
        public static double Add(double num1, double num2)
        {
            if (num1 == double.MaxValue || num2 == double.MaxValue) {
                throw new OverflowException();
            }
            return num1+num2;
        }

        public static double Substract(double num1, double num2)
        {
            return num1 - num2;
        }

        public static double Multiply(double num1, double num2)
        {
            return num1 * num2;

        }

        public static double Divide(double num1, double num2)
        {
            if(num2 == 0)
            {
                throw new DivideByZeroException();
            }
            return num1 / num2;
        }
    }
}
