﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace myTests
{
    public class MyTestingAreaTests
    {
        [Theory]
        [InlineData(3, 5, 8)]
        [InlineData(2.5, 25, 27.5)]
        public static void Add_FindSumOfNumbers(double num1, double num2, double expected)
        {
            //Arrange
            //Set up test data
            num1 = 3;
            num2 = 5;
            expected = 8;

            //Act
            //Use the method in question
            double actual = Calculator.Add(num1, num2);

            //Assert
            //Check that the results are correct
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5, 3, 2)]
        [InlineData(2.5, 25, -22.5)]
        public void Substract_FindSubstractOfNumbers(double num1, double num2, double expected)
        {
            //Arrange
            //Set up test data

            //Act
            //Use the method in question
            double actual = Calculator.Substract(num1,num2);

            //Assert
            //Check that the results are correct
            Assert.Equal(expected, actual);
            
        }

        [Theory]
        [InlineData(2, 2, 4)]
        [InlineData(3.2, 4, 12.8)]
        public void Multiply_ShouldFindMultiplyOfNumbers(double num1, double num2, double expected)
        {
            //Arrange
            //Set up test data

            //Act
            //Use the method in question
            double actual = Calculator.Multiply(num1, num2);

            //Assert
            //Check that the results are correct
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(6, 2 , 3)]
        [InlineData(4, 4, 1)]
        public void Divide_ShouldFindDivideOfNumbers(double num1, double num2, double expected)
        {
            //Arrange
            //Set up test data

            //Act
            //Use the method in question
            double actual = Calculator.Divide(num1, num2);

            //Assert
            //Check that the results are correct
            Assert.Equal(expected, actual);
        }


        // tests for two possible failures 
        //first when you try to divide by zero
        [Fact]
        public void Divied_ThrowsZeroException()
        {
            Assert.Throws<DivideByZeroException>(() => Calculator.Divide(4,0));
        }

        //second when the double exeeds the max value
        [Fact]
        public void Add_ThrowsOverflowException()
        {
           Assert.Throws<OverflowException>(() => Calculator.Add(double.MaxValue, double.MaxValue));
        }
    }
}
